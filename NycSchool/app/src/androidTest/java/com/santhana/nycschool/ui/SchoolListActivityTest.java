
package com.santhana.nycschool.ui;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertTrue;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.santhana.nycschool.R;
import com.santhana.nycschool.TestUtils;
import com.santhana.nycschool.adapter.SchoolListAdapter;

import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.action.ViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.RecyclerView;
import android.view.View;

@RunWith(AndroidJUnit4.class)
public class SchoolListActivityTest {

    private static Intent LAUNCH_INTENT = new Intent(
            InstrumentationRegistry.getTargetContext(),
            SchoolListActivity.class);

    @Rule
    public ActivityTestRule<SchoolListActivity> mActivityTestRule = new ActivityTestRule(
            SchoolListActivity.class) {
        @Override
        protected Intent getActivityIntent() {
            return LAUNCH_INTENT;
        }
    };

    @Test
    public void ensureSchoolListShown() {
        onView(withId(R.id.school_list_view)).check(matches((isDisplayed())));

        SchoolListActivity activity = mActivityTestRule.getActivity();
        View viewById = activity.findViewById(R.id.school_list_view);
        assertThat("Recycler view is present  ", viewById, notNullValue());
        assertThat(viewById, instanceOf(RecyclerView.class));
    }

    @Test
    public void ensureSchoolAdapter() {
        SchoolListActivity activity = mActivityTestRule.getActivity();
        View viewById = activity.findViewById(R.id.school_list_view);

        RecyclerView listView = (RecyclerView) viewById;
        RecyclerView.Adapter adapter = listView.getAdapter();

        assertThat("Adapter type test ", adapter, instanceOf(SchoolListAdapter.class));

    }

    @Test
    public void ensurePaginationCountMatching() {
        SchoolListActivity activity = mActivityTestRule.getActivity();
        View viewById = activity.findViewById(R.id.school_list_view);

        RecyclerView listView = (RecyclerView) viewById;
        RecyclerView.Adapter adapter = listView.getAdapter();

        TestUtils.delay(2000);
        assertThat(
                "Matches Page Count",
                adapter.getItemCount(),
                is(SchoolListActivity.SCHOOL_LIST_PAGINATION_THRESHOLD));
    }

    @Test
    public void isSchoolListActivityLaunched() {
        mActivityTestRule.launchActivity(LAUNCH_INTENT);
        TestUtils.delay(1000);
    }

    @Test
    public void testBackKeyToMoveHomeScreen() {
        onView(withId(R.id.action_btn_left)).check(matches((isDisplayed())));
        onView(withId(R.id.action_btn_left)).perform(ViewActions.click());

        assertTrue(mActivityTestRule.getActivity().isFinishing());
        TestUtils.waitUntilDestroy(mActivityTestRule.getActivity());
        assertTrue(mActivityTestRule.getActivity().isDestroyed());
    }
}
