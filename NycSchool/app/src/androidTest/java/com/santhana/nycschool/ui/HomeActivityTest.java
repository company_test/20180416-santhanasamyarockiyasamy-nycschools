
package com.santhana.nycschool.ui;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.junit.Assert.assertNotNull;

import org.junit.Rule;
import org.junit.Test;

import com.santhana.nycschool.R;

import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;

public class HomeActivityTest {

    public static Intent LAUNCH_INTENT = new Intent(
            InstrumentationRegistry.getTargetContext(),
            HomeActivity.class);

    @Rule
    public ActivityTestRule<HomeActivity> mActivityTestRule = new ActivityTestRule(
            HomeActivity.class,
            false,
            false) {
        @Override
        protected Intent getActivityIntent() {
            return LAUNCH_INTENT;
        }
    };

    @Test
    public void dashboardContainsSchoolOption() {
        assertNotNull(onView(withText(R.string.dash_action_school_list)));
    }

    @Test
    public void dashboardContainsScoreOption() {
        assertNotNull(onView(withText(R.string.dash_action_sat_score)));
    }

    @Test
    public void isHomeActivityLaunched() {
        mActivityTestRule.launchActivity(LAUNCH_INTENT);
    }

}
