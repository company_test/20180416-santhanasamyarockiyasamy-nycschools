
package com.santhana.nycschool.ui;

import static android.support.test.InstrumentationRegistry.getTargetContext;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

import org.junit.Rule;
import org.junit.Test;

import com.santhana.nycschool.R;
import com.santhana.nycschool.TestUtils;

import android.content.Intent;
import android.support.test.espresso.intent.Intents;
import android.support.test.rule.ActivityTestRule;

public class SplashActivityTest {

    private static Intent LAUNCH_INTENT = new Intent(getTargetContext(), SplashActivity.class);

    @Rule
    public ActivityTestRule<SplashActivity> mActivityTestRule = new ActivityTestRule(
            SplashActivity.class,
            false,
            false) {
        @Override
        protected Intent getActivityIntent() {
            return LAUNCH_INTENT;
        }
    };

    @Test
    public void splashLogoTest() {
        mActivityTestRule.launchActivity(LAUNCH_INTENT);
        TestUtils.delay(1000);
        onView(withId(R.id.splash_screen)).check(matches((isDisplayed())));
    }

    @Test
    public void isSplashScreenLaunched() {
        mActivityTestRule.launchActivity(LAUNCH_INTENT);
    }

    @Test
    public void splashScreenToHomeScreenTransitionTest() {
        mActivityTestRule.launchActivity(LAUNCH_INTENT);

        Intents.init();
        // assertTrue(mActivityTestRule.getActivity().isFinishing());
        TestUtils.waitUntilDestroy(mActivityTestRule.getActivity());
        intended(hasComponent(HomeActivity.class.getName()));
        Intents.release();
    }
}
