
package com.santhana.nycschool;

import android.app.Activity;
import android.app.Instrumentation;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.intent.matcher.IntentMatchers;

import com.santhana.nycschool.ui.HomeActivity;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;

public class TestUtils {

    public static void delay( long aDelayInMs ) {
        CountDownLatch lLatch = new CountDownLatch(1);
        try {
            lLatch.await(aDelayInMs, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void waitUntilDestroy( Activity activity ) {

        while (!activity.isDestroyed()) {
            TestUtils.delay(1000);
        }

    }

    public static Activity waitUntilAppear( String aComponentName ) {

        return getInstrumentation().waitForMonitorWithTimeout(
                getInstrumentation().addMonitor(aComponentName, null, false),
                5000);

    }

    public static NycApplication getApplication() {
        return (NycApplication) InstrumentationRegistry
                .getInstrumentation()
                .getTargetContext()
                .getApplicationContext();
    }
}
