
package com.santhana.nycschool.ui;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.assertTrue;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.santhana.nycschool.R;
import com.santhana.nycschool.TestUtils;

import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.action.ViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

@RunWith(AndroidJUnit4.class)
public class ScoreListActivityTest {

    private static Intent LAUNCH_INTENT = new Intent(
            InstrumentationRegistry.getTargetContext(),
            ScoreListActivity.class);

    @Rule
    public ActivityTestRule<ScoreListActivity> mActivityTestRule = new ActivityTestRule(
            ScoreListActivity.class) {
        @Override
        protected Intent getActivityIntent() {
            return LAUNCH_INTENT;
        }
    };

    @Test
    public void isSATScoreListActivityLaunched() {
        mActivityTestRule.launchActivity(LAUNCH_INTENT);
    }

    @Test
    public void testBackKeyToMoveHomeScreen() {
        onView(withId(R.id.action_btn_left)).check(matches((isDisplayed())));
        onView(withId(R.id.action_btn_left)).perform(ViewActions.click());
        assertTrue(mActivityTestRule.getActivity().isFinishing());
        TestUtils.waitUntilDestroy(mActivityTestRule.getActivity());
        assertTrue(mActivityTestRule.getActivity().isDestroyed());
    }
}
