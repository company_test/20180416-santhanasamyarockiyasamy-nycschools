
package com.santhana.nycschool;

/**
 * Class to hold Constants related to application level.
 */
public class AppConstant {

    /**
     * Handy Application context
     */
    public static NycApplication APP_CONTEXT;

    /**
     * Splash screen timeout
     */
    public static final long SPLASH_DELAY_TIME = 2000;

    /**
     * Name of the Database
     */
    public static final String NYC_DB_NAME = "BigCommerce.db";

    /**
     * Base URI
     */
    public static final String APP_BASE_URI_STR = "https://data.cityofnewyork.us/";

    /**
     * Query String to get the available data count from the server
     */
    public static final String COUNT_QUERY_STRING = "select count(*)  as resultCount";

    /**
     * Path to the NYC High School Directory<br/>
     * https://data.cityofnewyork.us/resource/s3k6-pzi2.json
     */
    public static final String SCHOOL_INFO_URI = "resource/s3k6-pzi2.json";

    public static final String SCHOOL_INFO_FILTER_URI = "api/id/s3k6-pzi2.json";

    public static final String SCHOOL_COUNT_URI = "api/id/s3k6-pzi2";

    /**
     * Path to get the SAT Result for the school's in NYC <br/>
     * https://data.cityofnewyork.us/resource/f9bf-2cp4.json
     */
    public static final String SAT_SCORE_URI_STR = "api/id/f9bf-2cp4.json";

    public static final String SAT_SCORE_COUNT_URI = "api/id/f9bf-2cp4";

    public static final String SAT_SCORE_BY_ID_URI = "api/id/f9bf-2cp4";


    // https://data.cityofnewyork.us/api/id/f9bf-2cp4?dbn=01M292

    public interface NYCSchoolFilter {
        String SELECT = "$select";

        String ORDER = "$order";

        String LIMIT = "$limit";

        String OFFSET = "$offset";

        String SORT_ORDER = ":id ASC";

        String SCHOOL_DBN_ID = "dbn";

    }

    /**
     * NYC School list projection
     */
    public static final String NYC_SCHOOL_QUERY_PARAM = "dbn,school_name,school_email,school_sports,overview_paragraph,location,phone_number,fax_number,website,zip,start_time,end_time,bus,subway";

    /**
     * NYC Schools Score info projection
     */
    public static final String NYC_SCHOOL_SCORE_QUERY_PARAM = "dbn,school_name,num_of_sat_test_takers,sat_critical_reading_avg_score,sat_math_avg_score,sat_writing_avg_score";

}
