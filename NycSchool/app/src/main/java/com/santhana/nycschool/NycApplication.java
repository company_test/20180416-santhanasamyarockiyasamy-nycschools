
package com.santhana.nycschool;

import com.santhana.nycschool.di.ApplicationComponent;
import com.santhana.nycschool.di.ApplicationModule;
import com.santhana.nycschool.di.DaggerApplicationComponent;

import android.app.Application;

/**
 * NYC-Application class.
 */
public class NycApplication extends Application {

    private ApplicationComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        // Build & Initialize dagger component with supported modules.
        appComponent = DaggerApplicationComponent
                .builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        AppConstant.APP_CONTEXT = this;
    }

    public ApplicationComponent getApplicationComponent() {
        return appComponent;
    }
}
