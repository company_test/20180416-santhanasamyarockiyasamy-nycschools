
package com.santhana.nycschool.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.VisibleForTesting;

import com.santhana.nycschool.models.ResponseResultCount;
import com.santhana.nycschool.models.Score;
import com.santhana.nycschool.repo.NYCSchoolRepo;

import java.util.List;

public class ScoreInfoViewModel extends ViewModel {

    private NYCSchoolRepo mNYCSchoolRepo;

    private LiveData<List<Score>> mScoreListData;

    private LiveData<List<ResponseResultCount>> mScoreCountData;

    public ScoreInfoViewModel(NYCSchoolRepo repo) {
        this.mNYCSchoolRepo = repo;
        // mSchoolListData = repo.getNYCHighSchoolListFromDB();
    }

    @VisibleForTesting
    public LiveData<List<ResponseResultCount>> getScoreResultSetSize() {

        if (null == mScoreCountData) {
            mScoreCountData = mNYCSchoolRepo.getNYCSchoolScoreCount();
        }
        return mScoreCountData;
    }

    @VisibleForTesting
    public LiveData<List<Score>> getScoreList() {

        if (null == mScoreListData) {
            mScoreListData = mNYCSchoolRepo.getNYCSchoolScoreList();
        }
        return mScoreListData;
    }

    @VisibleForTesting
    public LiveData<List<Score>> getScoreList(String aDBNId) {
        return mNYCSchoolRepo.getNYCSchoolScoreByID(aDBNId);
    }

    @VisibleForTesting
    public LiveData<List<Score>> getPaginatedNYCSchoolScoreList(int aOffset, int aLimit) {
        return mNYCSchoolRepo.getPaginatedNYCSchoolScoreList(aOffset, aLimit);
    }

}
