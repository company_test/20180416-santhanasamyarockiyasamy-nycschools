
package com.santhana.nycschool.models;

/**
 * Model to represent the size of the queried data set for NYC Schools & Score
 */
public class ResponseResultCount {
    private String resultCount;

    public String getResultCount() {
        return resultCount;
    }

    public void setResultCount( String resultCount ) {
        this.resultCount = resultCount;
    }

    @Override
    public String toString() {
        return "ClassPojo [resultCount = " + resultCount + "]";
    }
}
