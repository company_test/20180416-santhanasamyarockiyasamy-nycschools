
package com.santhana.nycschool.repo;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.santhana.nycschool.models.School;
import com.santhana.nycschool.models.Score;

@Database(entities = {
        School.class, Score.class
}, version = 1, exportSchema = false)
/**
 * Class to create data base to store NYC school and score detailed fetched from the school repo.
 */
public abstract class NYCSchoolDatabase extends RoomDatabase {

    public abstract SchoolInfoDAO scoreInfoDAO();
}
