
package com.santhana.nycschool.ui;

import com.santhana.nycschool.R;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewParent;

/**
 * Base activity which will provide common implementation to the activities
 * available in the NYC application.
 */
public class BaseActivity extends AppCompatActivity implements View.OnClickListener {

    protected int mActionBarViewID;

    protected ActionBarView mActionBarLayout;

    @Override
    protected void onCreate( @Nullable Bundle savedInstanceState ) {
        // ((NycApplication) getApplication()).getApplicationComponent().inject(this);
        super.onCreate(savedInstanceState);
        configureActionBar();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        CustomProgressDialog.hideProgressBar();
    }

    /**
     * Method to configure action bar based on the current need.
     */
    protected void configureActionBar() {

        ActionBar lActionBar = getSupportActionBar();
        if (null == lActionBar) {
            return;
        }

        mActionBarViewID = getActionBarLayoutID();

        if (-1 == mActionBarViewID) {
            mActionBarLayout = new ActionBarView(this);
        }

        if (-1 != mActionBarViewID) {
            lActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            lActionBar.setCustomView(mActionBarViewID);
            mActionBarLayout = (ActionBarView) lActionBar.getCustomView();
            if (null != mActionBarLayout && isBackButtonNeeded()) {
                mActionBarLayout.setLeftActionImage(R.drawable.ic_back_arrow_white);
            }
        } else if (null != mActionBarLayout) {
            lActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            lActionBar.setCustomView(mActionBarLayout);
            ViewParent lParent = mActionBarLayout.getParent();
            if (null != lParent && lParent instanceof Toolbar) {
                ((Toolbar) lParent).setContentInsetsAbsolute(0, 0);
            }
            if (isBackButtonNeeded()) {
                mActionBarLayout.setLeftActionImage(R.drawable.ic_back_arrow_white);
                mActionBarLayout.getLeftActionView().setOnClickListener(this);
            }
            mActionBarLayout.getRightActionView().setOnClickListener(this);
        } else {
            if (isBackButtonNeeded()) {
                lActionBar.setDisplayHomeAsUpEnabled(true);
            }
        }

    }

    /**
     * Check whether the back button is needed to navigate back from the current
     * activity
     *
     * @return
     */
    protected boolean isBackButtonNeeded() {
        return false;
    }

    /**
     * Provide id of the layout which will be inflated in the place of default
     * action bar.
     *
     * @return
     */
    protected int getActionBarLayoutID() {
        return -1;
    }

    @Override
    public void onClick( View aView ) {
        int lID = aView.getId();
        if (R.id.action_btn_left == lID) {
            onLeftActionClicked(aView);
        } else if (R.id.action_txt_right == lID) {
            onRightActionClicked(aView);
        }
    }

    /**
     * Method to be called when the right action icon on the action bar is clicked.
     *
     * @param aRightActionTxtView
     */
    protected void onRightActionClicked( View aRightActionTxtView ) {

    }

    /**
     * Method to be called when the left action icon on the action bar is clicked.
     *
     * @param aRightActionTxtView
     */
    protected void onLeftActionClicked( View aRightActionTxtView ) {

        finish();
    }
}
