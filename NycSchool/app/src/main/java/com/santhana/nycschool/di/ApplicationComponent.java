
package com.santhana.nycschool.di;

import javax.inject.Singleton;

import com.santhana.nycschool.NycApplication;
import com.santhana.nycschool.ui.HomeActivity;
import com.santhana.nycschool.ui.SchoolListActivity;
import com.santhana.nycschool.ui.ScoreListActivity;
import com.santhana.nycschool.ui.SchoolDetailActivity;
import com.santhana.nycschool.ui.SplashActivity;

import dagger.Component;
import dagger.android.AndroidInjector;

@Singleton
@Component(modules = {
        ApplicationModule.class
})
/**
 * Dagger component
 */
public interface ApplicationComponent extends AndroidInjector<NycApplication> {

    void inject( HomeActivity activity );

    void inject( SplashActivity activity );

    void inject( SchoolListActivity activity );

    void inject( ScoreListActivity activity );

    void inject( SchoolDetailActivity activity );

}
