
package com.santhana.nycschool.utils;

import java.io.File;
import java.util.Arrays;
import java.util.logging.Level;

import com.santhana.nycschool.BuildConfig;

import android.app.Application;
import android.os.Environment;
import android.text.TextUtils;

import pl.brightinventions.slf4android.FileLogHandlerConfiguration;
import pl.brightinventions.slf4android.LogLevel;
import pl.brightinventions.slf4android.LoggerConfiguration;

public class Logger {

    public static final String LOGGER_NAME = "NycSchool";

    public static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(LOGGER_NAME);

    public static final boolean DEBUG_LOG = true;// BuildConfig.DEBUG;

    public static final boolean ERROR_LOG = true;// BuildConfig.DEBUG;

    public static final boolean INFO_LOG = true;

    public static final boolean EXCEPTION_LOG = true;// BuildConfig.DEBUG;

    public static String LOG_FILE_PATH = "";

    public static boolean REMOTE_DEBUG = true;

    private Logger() {
    }

    public static boolean isLogEnabled() {
        return BuildConfig.DEBUG;
    }

    public static final void configureLogging( Application aContext ) {

        // 1. Internal file store
        FileLogHandlerConfiguration lConfig = LoggerConfiguration.fileLogHandler(aContext);

        String lLogPath = "";
        try {
            File lExternalStorage = aContext.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS);
            if (!lExternalStorage.exists()) {
                lExternalStorage.mkdir();
            }
            File lFile = new File(lExternalStorage, LOGGER_NAME + ".%g.%u.log");
            lLogPath = lFile.getAbsolutePath();
        } catch (Exception exe) {

        }
        if (!TextUtils.isEmpty(lLogPath)) {
            lConfig.setFullFilePathPattern(lLogPath);
        }
        lConfig.setRotateFilesCountLimit(1);
        lConfig.setLogFileSizeLimitInBytes(524288);
        lConfig.setLevel(Level.ALL);

        LoggerConfiguration
                .configuration()
                .setLogLevel(LOGGER_NAME, LogLevel.TRACE)
                .addHandlerToLogger(LOGGER_NAME, lConfig);
        LOG_FILE_PATH = lConfig.getCurrentFileName();

        // System.out.println("[ <---- Logs ----> ][" + LOG_FILE_PATH + "]");
    }

    public static final void printDLog( String aTAG, String aMsg, String... aStr ) {
        if (!DEBUG_LOG) {
            return;
        }
        if (null == aStr || aStr.length == 0) {
            LOG.debug(aTAG + " [" + aMsg + "]");
        } else {
            LOG.debug(aTAG + " [" + aMsg + "]" + Arrays.asList(aStr));
        }
    }

    public static final void printELog( String aTAG, String aMsg, String... aStr ) {

        if (!ERROR_LOG) {
            return;
        }
        if (null == aStr || aStr.length == 0) {
            LOG.error(aTAG + " [" + aMsg + "]");
        } else {
            LOG.error(aTAG + " [" + aMsg + "]" + Arrays.asList(aStr));
        }

    }

    public static final void printILog( String aTAG, String aMsg, String... aStr ) {

        if (!INFO_LOG) {
            return;
        }
        if (null == aStr || aStr.length == 0) {
            LOG.info(aTAG + " [" + aMsg + "]");
        } else {
            LOG.info(aTAG + " [" + aMsg + "]" + Arrays.asList(aStr));
        }
    }

    public static final void printSys( String aMsg, String... aStr ) {

        if (!DEBUG_LOG) {
            return;
        }
        int length = aStr.length;

        if (length == 1) {
            System.out.println("[" + aMsg + "][" + aStr[0] + "]");
        } else if (length == 2) {
            System.out.println("[" + aMsg + "][" + aStr[0] + "," + aStr[1] + "]");
        } else if (length == 3) {
            System.out.println("[" + aMsg + "][" + aStr[0] + "," + aStr[1] + "," + aStr[2] + "]");
        } else {
            System.out.println("[" + aMsg + "]" + Arrays.asList(aStr));
        }
    }

    public static final void printException( String aTAG, String aMsg, Exception aExc ) {

        if (null == aExc) {
            return;
        }

        LOG.error(aTAG + " [Exception][" + aMsg + "][" + aExc.getMessage() + "]");

        if (!EXCEPTION_LOG) {
            return;
        }

        LOG.error(aTAG + " [" + aMsg + " : EXCEPTION - TRACE ]");
        printStackTrace(aExc);
    }

    public static final void printException( String aTAG, Exception aException ) {

        if (null == aException) {
            return;
        }
        LOG.error(aTAG + " [Exception] [" + aException.getMessage() + "]");

        if (!EXCEPTION_LOG) {
            return;
        }

        printStackTrace(aException);
    }

    public static void printStackTrace( Exception e ) {
        if (DEBUG_LOG) {
            e.printStackTrace();
        }
    }

}
