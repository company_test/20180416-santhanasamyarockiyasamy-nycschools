
package com.santhana.nycschool.utils;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.santhana.nycschool.R;

/**
 * Utilities for User Interface.
 */
public class UiUtils {

    /**
     * Handy method to configure the Dash Board Items
     *
     * @param aListener
     * @param lView
     * @param titleTxt1
     * @param titleImg1
     * @param titleTxt2
     * @param titleImg2
     */
    public static void setGrid(
            View.OnClickListener aListener,
            View lView,
            int titleTxt1,
            int titleImg1,
            int titleTxt2,
            int titleImg2) {
        if (-1 == titleTxt1 && -1 == titleImg1 && -1 == titleTxt2 && -1 == titleImg2) {
            lView.setVisibility(View.GONE);
            return;
        }
        View lContainer1 = lView.findViewById(R.id.col_1_container);
        lContainer1.setVisibility(View.VISIBLE);
        if (-1 != titleTxt1 && -1 != titleImg1) {
            lContainer1.setTag(titleTxt1);
            lContainer1.setOnClickListener(aListener);
            TextView lBtn1 = lView.findViewById(R.id.btn_1);
            lBtn1.setText(titleTxt1);
            ImageView lImg1 = lView.findViewById(R.id.img_view_1);
            lImg1.setImageResource(titleImg1);
            lImg1.bringToFront();
        } else {
            lContainer1.setVisibility(View.INVISIBLE);
        }

        View lContainer2 = lView.findViewById(R.id.col_2_container);
        lContainer2.setVisibility(View.VISIBLE);
        if (-1 != titleTxt2 && -1 != titleImg2) {
            lContainer2.setTag(titleTxt2);
            lContainer2.setOnClickListener(aListener);
            TextView lBtn2 = lView.findViewById(R.id.btn_2);
            lBtn2.setText(titleTxt2);
            ImageView lImg2 = lView.findViewById(R.id.img_view_2);
            lImg2.setImageResource(titleImg2);
            lImg2.bringToFront();
        } else {
            lContainer2.setVisibility(View.INVISIBLE);
        }
    }

}
