
package com.santhana.nycschool.ui;

import com.santhana.nycschool.R;
import com.santhana.nycschool.utils.Logger;

import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

/**
 * Custom Alert Dialog - Configured based on the app's theme.
 */
public class CustomProgressDialog extends Dialog {
    private static String TAG = CustomProgressDialog.class.getSimpleName();

    private static CustomProgressDialog sProgressDialog;

    private final TextView mProgressMsgView;

    private CustomProgressDialog(Context context) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        View lView = LayoutInflater.from(context).inflate(R.layout.custom_progress_dialog, null);
        mProgressMsgView = lView.findViewById(R.id.progress_msg_view);
        setContentView(lView);
        this.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }

    /**
     * Method to show the progress bar
     *
     * @param context Activity context
     * @param cancelable True if the progress bar is cancelable while tapping on the
     *            screen
     */
    public static void showProgressBar( Context context, boolean cancelable ) {
        showProgressBar(context, cancelable, null);
    }

    /**
     * Method to show the progress bar
     * 
     * @param context Activity context
     * @param cancelable True if the progress bar is cancelable while tapping on the
     *            screen
     * @param message Message to be displayed below the progress wheel.
     */
    public static void showProgressBar( Context context, boolean cancelable, String message ) {

        try {
            if (sProgressDialog != null && sProgressDialog.isShowing()) {
                sProgressDialog.cancel();
            }
            sProgressDialog = new CustomProgressDialog(context);
            sProgressDialog.setCancelable(cancelable);

            TextView lView = sProgressDialog.getMessageTxtView();
            if (TextUtils.isEmpty(message)) {
                lView.setVisibility(View.GONE);
            } else {
                lView.setVisibility(View.VISIBLE);
                lView.setText(message);
            }
            sProgressDialog.show();
        } catch (Exception aException) {
            Logger.printELog(TAG, "Error while showing dialog");
        }
    }

    private TextView getMessageTxtView() {
        return mProgressMsgView;
    }

    /**
     * Method to hide the progress bar
     */
    public static void hideProgressBar() {
        try {
            if (sProgressDialog != null && sProgressDialog.isShowing()) {
                sProgressDialog.dismiss();
            }
        } catch (Exception e) {
            Logger.printELog(TAG, "Error while closing dialog");
        }
    }
}
