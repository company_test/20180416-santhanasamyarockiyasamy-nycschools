
package com.santhana.nycschool.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.santhana.nycschool.R;
import com.santhana.nycschool.databinding.SchoolListItemBinding;
import com.santhana.nycschool.models.School;

import java.util.List;

public class SchoolListAdapter
        extends RecyclerView.Adapter<SchoolListAdapter.SchoolInfoViewHolder> {

    private List<School> mNYCSchoolList;

    private Context mContext;

    private ResultsClickListener resultsClickListener;

    public interface ResultsClickListener {
        void onResultItemClicked(School aSchool);
    }

    public SchoolListAdapter(@NonNull Context context, ResultsClickListener resultsClickListener) {
        this.mContext = context;
        this.resultsClickListener = resultsClickListener;
    }

    public void setNYCSchoolList(List<School> aSchoolList) {
        this.mNYCSchoolList = aSchoolList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public SchoolInfoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SchoolInfoViewHolder(
                DataBindingUtil.inflate(
                        LayoutInflater.from(mContext),
                        R.layout.school_list_item,
                        parent,
                        false));
    }

    @Override
    public void onBindViewHolder(@NonNull SchoolInfoViewHolder holder, int position) {
        holder.resultsRowBinding.setSchoolInfo(mNYCSchoolList.get(position));
    }

    @Override
    public int getItemCount() {
        return mNYCSchoolList == null || mNYCSchoolList.isEmpty() ? 0 : mNYCSchoolList.size();
    }

    public class SchoolInfoViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        private SchoolListItemBinding resultsRowBinding;

        public SchoolInfoViewHolder(SchoolListItemBinding resultsRowBinding) {
            super(resultsRowBinding.getRoot());
            this.resultsRowBinding = resultsRowBinding;
            resultsRowBinding.getRoot().setOnClickListener(this);
        }

        public void setSchoolInfo(School aSchoolInfo) {
            resultsRowBinding.setSchoolInfo(aSchoolInfo);
        }

        @Override
        public void onClick(View v) {
            resultsClickListener.onResultItemClicked(mNYCSchoolList.get(getAdapterPosition()));
        }
    }
}
