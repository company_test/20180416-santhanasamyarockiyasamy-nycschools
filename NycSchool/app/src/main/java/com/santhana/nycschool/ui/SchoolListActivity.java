
package com.santhana.nycschool.ui;

import javax.inject.Inject;

import com.santhana.nycschool.NycApplication;
import com.santhana.nycschool.R;
import com.santhana.nycschool.adapter.SchoolListAdapter;
import com.santhana.nycschool.databinding.ActivitySchoolListBinding;
import com.santhana.nycschool.utils.AppUtils;
import com.santhana.nycschool.utils.Logger;
import com.santhana.nycschool.viewmodel.SchoolInfoViewModel;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Screen to show the NYC high schools.
 */
public class SchoolListActivity extends BaseActivity {

    private static final String TAG = SchoolListActivity.class.getSimpleName();

    public static final int SCHOOL_LIST_PAGINATION_THRESHOLD = 10;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    private ActivitySchoolListBinding mBinding = null;

    private SchoolInfoViewModel mSchoolInfoViewModel = null;

    private SchoolListAdapter mSchoolListAdapter = null;

    private int mStart = 0, mEnd = 0, mTotalCount = 0;

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        ((NycApplication) getApplication()).getApplicationComponent().inject(this);

        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_school_list);

        mSchoolInfoViewModel = ViewModelProviders.of(this, viewModelFactory).get(
                SchoolInfoViewModel.class);

        // Get All High school in NYC
        mSchoolInfoViewModel.getSchoolList().observe(this, nycSchools -> {
            if (null == nycSchools) {
                return;
            }
            Logger.printILog(TAG, " Result Fetched -> " + nycSchools.size());
        });

        // Get the count of High schools in NYC
        mSchoolInfoViewModel.getSchoolResultSetSize().observe(this, resultCount -> {
            if (null == resultCount) {
                return;
            }
            mTotalCount = Integer.parseInt(resultCount.get(0).getResultCount());
            configurePaginationTxt();
        });

        // Prepare adapter to bind the DataSet with UI
        mSchoolListAdapter = new SchoolListAdapter(this, nycSchool -> {

            if (null == nycSchool) {
                return;
            }
            SchoolDetailActivity.launchSchoolDetail(this, nycSchool);
            Logger.printDLog(TAG, "Clicked School Info " + nycSchool.toString());
        });

        // Get first 10 High schools in NYC
        getSchoolListAsPages(mStart, SCHOOL_LIST_PAGINATION_THRESHOLD);

        // Configure Recycler view
        RecyclerView.LayoutManager lManager = new LinearLayoutManager(this);
        mBinding.schoolListView.setLayoutManager(lManager);
        // ItemDecoration for each item in the list
        mBinding.schoolListView
                .addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        mBinding.schoolListView.setAdapter(mSchoolListAdapter);

        mBinding.setViewModel(mSchoolInfoViewModel);

        mBinding.footerLayout.rightImgView.setOnClickListener(this);
        mBinding.footerLayout.leftImgView.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        configurePaginationTxt();
    }

    @Override
    protected boolean isBackButtonNeeded() {
        return true;
    }

    @Override
    protected void configureActionBar() {
        super.configureActionBar();
        mActionBarLayout.setLeftTitle(R.string.dash_action_school_list);
    }

    @Override
    public void onClick( View aView ) {
        super.onClick(aView);

        int lViewID = aView.getId();

        if (R.id.left_img_view == lViewID) {

            if (!canDecrement()) {
                mBinding.footerLayout.leftImgView.setEnabled(false);
                return;
            }
            mBinding.footerLayout.rightImgView.setEnabled(canIncrement());

            mStart = mStart - SCHOOL_LIST_PAGINATION_THRESHOLD;
            mEnd = mStart + SCHOOL_LIST_PAGINATION_THRESHOLD;

            configurePaginationTxt();
            getSchoolListAsPages(mStart, SCHOOL_LIST_PAGINATION_THRESHOLD);
        } else if (R.id.right_img_view == lViewID) {

            if (!canIncrement()) {
                mBinding.footerLayout.rightImgView.setEnabled(false);
                return;
            }
            mBinding.footerLayout.leftImgView.setEnabled(canDecrement());

            mStart = mEnd;
            mEnd = mEnd + SCHOOL_LIST_PAGINATION_THRESHOLD;
            if (mEnd > mTotalCount) {
                mEnd -= (mEnd - mTotalCount);
            }

            configurePaginationTxt();
            getSchoolListAsPages(mStart, SCHOOL_LIST_PAGINATION_THRESHOLD);
        }
    }

    public boolean canIncrement() {
        return (mEnd + SCHOOL_LIST_PAGINATION_THRESHOLD) < mTotalCount;
    }

    public boolean canDecrement() {
        return ((mStart - SCHOOL_LIST_PAGINATION_THRESHOLD) >= 0);
    }

    private void configurePaginationTxt() {

        String lStart = "" + (mStart + 1);
        String lEnd = "" + (mStart + SCHOOL_LIST_PAGINATION_THRESHOLD);
        String lSize = "" + mTotalCount;
        mBinding.footerLayout.paginationTxtView.setText(
                String.format(
                        getString(R.string.str_school_list_pagination_info),
                        lStart,
                        lEnd,
                        lSize));

        Logger.printDLog(TAG, " Result Set Size -> " + lSize);
    }

    private void getSchoolListAsPages( int aOffset, int aLimit ) {

        CustomProgressDialog
                .showProgressBar(this, false, getString(R.string.str_warning_please_wait));
        Logger.printDLog(TAG, "Paginating", " From : " + aOffset, " Limiting : " + aLimit);
        mSchoolInfoViewModel.getFilteredSchoolList(aOffset, aLimit).observe(this, nycSchools -> {

            if (null == nycSchools) {
                CustomProgressDialog.hideProgressBar();
                AppUtils.showShortToast(R.string.str_warning_failed_to_get_school);
                return;
            }
            Logger.printDLog(TAG, " Result Fetched (Filter) -> " + nycSchools.size());
            mSchoolListAdapter.setNYCSchoolList(nycSchools);

            CustomProgressDialog.hideProgressBar();
        });
    }
}
