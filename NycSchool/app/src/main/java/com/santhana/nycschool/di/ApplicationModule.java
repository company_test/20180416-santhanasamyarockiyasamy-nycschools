
package com.santhana.nycschool.di;

import javax.inject.Named;
import javax.inject.Singleton;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.santhana.nycschool.AppConstant;
import com.santhana.nycschool.NycApplication;
import com.santhana.nycschool.repo.NYCSchoolDatabase;
import com.santhana.nycschool.repo.NYCSchoolRepo;
import com.santhana.nycschool.repo.SchoolInfoDAO;
import com.santhana.nycschool.service.ApiService;
import com.santhana.nycschool.utils.RetrofitAPIKeyInterceptor;
import com.santhana.nycschool.viewmodel.AppViewModelFactory;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.persistence.room.Room;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
/**
 * Dagger module which provides dependencies need for the NYC application.
 */
public class ApplicationModule {

    private final NycApplication application;

    public ApplicationModule(NycApplication application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Application provideApplication() {
        return application;
    }

    @Provides
    @Named("NYCDataBase")
    String providesNYCDBPath() {
        return AppConstant.NYC_DB_NAME;
    }

    @Singleton
    @Provides
    public NYCSchoolDatabase provideNYCSchoolDatabase(
            Application application,
            @Named("NYCDataBase") String dbName ) {
        return Room
                .databaseBuilder(application, NYCSchoolDatabase.class, dbName)
                .fallbackToDestructiveMigration()
                .build();

    }

    @Singleton
    @Provides
    SchoolInfoDAO provideFavoriteDao( NYCSchoolDatabase aDatabase ) {
        return aDatabase.scoreInfoDAO();
    }

    @Provides
    @Singleton
    NYCSchoolRepo provideNYCSchoolRepo( ApiService apiService, SchoolInfoDAO aFavouriteDao ) {
        return new NYCSchoolRepo(apiService, aFavouriteDao);
    }

    @Provides
    Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        return gsonBuilder.create();
    }

    @Provides
    @Singleton
    OkHttpClient.Builder provideHTTPInterceptor( Interceptor apiKeyInterceptor ) {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder lHttpClient = new OkHttpClient.Builder();
        lHttpClient.addInterceptor(apiKeyInterceptor);
        lHttpClient.addInterceptor(logging);

        return lHttpClient;
    }

    @Provides
    @Singleton
    Retrofit provideNetworkClient(
            OkHttpClient.Builder okHttpClientBuilder,
            @Named("baseUrl") String baseUrl ) {

        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                // .addCallAdapterFactory(new LiveDataCallAdapterFactory())
                .client(okHttpClientBuilder.build())
                .build();

    }

    @Provides
    @Singleton
    Interceptor provideApiKeyInterceptor() {
        return new RetrofitAPIKeyInterceptor();
    }

    @Provides
    @Named("baseUrl")
    String provideBaseUrl() {
        return AppConstant.APP_BASE_URI_STR;
    }

    @Provides
    @Singleton
    ApiService provideApiService( Retrofit retrofit ) {
        return retrofit.create(ApiService.class);
    }

    @Provides
    @Singleton
    ViewModelProvider.Factory provideViewModelFactory( NYCSchoolRepo repository ) {
        return new AppViewModelFactory(repository);
    }

    // @Provides
    // @Singleton
    // ViewModelProvider.Factory provideScoreViewModelFactory(NYCSchoolRepo
    // repository) {
    // return new ScoreInfoViewModelFactory(repository);
    // }
}
