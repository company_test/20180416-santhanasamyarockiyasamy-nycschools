
package com.santhana.nycschool.repo;

import java.util.List;

import javax.inject.Inject;

import com.santhana.nycschool.AppConstant;
import com.santhana.nycschool.models.School;
import com.santhana.nycschool.models.ResponseResultCount;
import com.santhana.nycschool.models.Score;
import com.santhana.nycschool.service.ApiService;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Application level Centralized repository to fetch & store data needed for the
 * NYC application.
 */
public class NYCSchoolRepo {

    private final ApiService mApiService;

    private final SchoolInfoDAO mSchoolInfoDAO;

    @Inject
    public NYCSchoolRepo(ApiService apiService, SchoolInfoDAO aSchoolInfoDAO) {
        mApiService = apiService;
        mSchoolInfoDAO = aSchoolInfoDAO;
    }

    /**
     * Method to get the High school list from the NYC repo
     * 
     * @return List of Nyc schools
     */
    public LiveData<List<School>> getNYCSchoolList() {
        final MutableLiveData<List<School>> data = new MutableLiveData<>();
        mApiService.getNYCSchoolList().enqueue(new Callback<List<School>>() {
            @Override
            public void onResponse( Call<List<School>> call, Response<List<School>> response ) {
                if (response.isSuccessful()) {
                    data.setValue(response.body());
                }
            }

            @Override
            public void onFailure( Call<List<School>> call, Throwable t ) {
                data.setValue(null);
            }
        });
        return data;
    }

    /**
     * Method to get the High school list from the NYC repo as pages
     * 
     * @param aOffset - Starting offset
     * @param aLimit - No of records from the starting offset
     * @return List of Nyc schools
     */
    public LiveData<List<School>> getFilteredNYCSchoolList( int aOffset, int aLimit ) {
        final MutableLiveData<List<School>> data = new MutableLiveData<>();
        mApiService
                .getPaginatedNYCSchoolList(
                        AppConstant.NYC_SCHOOL_QUERY_PARAM,
                        AppConstant.NYCSchoolFilter.SORT_ORDER,
                        "" + aLimit,
                        "" + aOffset)
                .enqueue(new Callback<List<School>>() {
                    @Override
                    public void onResponse(
                            Call<List<School>> call,
                            Response<List<School>> response ) {
                        if (response.isSuccessful()) {
                            data.setValue(response.body());
                        }
                    }

                    @Override
                    public void onFailure( Call<List<School>> call, Throwable t ) {
                        data.setValue(null);
                    }
                });
        return data;
    }

    /**
     * Method to get the all the High school list from the NYC repo.
     * 
     * @return List of Nyc schools
     */
    public LiveData<List<Score>> getNYCSchoolScoreList() {
        final MutableLiveData<List<Score>> data = new MutableLiveData<>();
        mApiService.getNYCSchoolScoreList().enqueue(new Callback<List<Score>>() {
            @Override
            public void onResponse( Call<List<Score>> call, Response<List<Score>> response ) {
                if (response.isSuccessful()) {
                    data.setValue(response.body());
                }
            }

            @Override
            public void onFailure( Call<List<Score>> call, Throwable t ) {
                data.setValue(null);
            }
        });
        return data;
    }

    /**
     * Method to get the SAT score of the supplied school ID
     *
     * @param aDBNId - School ID
     * @return SAT Score belongs to the requested school.
     */
    public LiveData<List<Score>> getNYCSchoolScoreByID( String aDBNId ) {
        final MutableLiveData<List<Score>> data = new MutableLiveData<>();
        mApiService.getNYCSchoolScoreByID(aDBNId).enqueue(new Callback<List<Score>>() {
            @Override
            public void onResponse( Call<List<Score>> call, Response<List<Score>> response ) {
                if (response.isSuccessful()) {
                    data.setValue(response.body());
                }
            }

            @Override
            public void onFailure( Call<List<Score>> call, Throwable t ) {
                data.setValue(null);
            }
        });
        return data;
    }

    /**
     * Method to get the High school score list from the NYC repo as pages
     *
     * @param aOffset - Starting offset
     * @param aLimit - No of records from the starting offset
     * @return List of Nyc schools SAT score
     */
    public LiveData<List<Score>> getPaginatedNYCSchoolScoreList( int aOffset, int aLimit ) {
        final MutableLiveData<List<Score>> data = new MutableLiveData<>();
        mApiService
                .getPaginatedNYCSchoolScoreList(
                        AppConstant.NYC_SCHOOL_QUERY_PARAM,
                        AppConstant.NYCSchoolFilter.SORT_ORDER,
                        "" + aLimit,
                        "" + aOffset)
                .enqueue(new Callback<List<Score>>() {
                    @Override
                    public void onResponse(
                            Call<List<Score>> call,
                            Response<List<Score>> response ) {
                        if (response.isSuccessful()) {
                            data.setValue(response.body());
                        }
                    }

                    @Override
                    public void onFailure( Call<List<Score>> call, Throwable t ) {
                        data.setValue(null);
                    }
                });
        return data;
    }

    /**
     * Get the no of High school records available in the NYC repo
     * 
     * @return High school record count in NYC
     */
    public LiveData<List<ResponseResultCount>> getNYCSchoolCount() {
        final MutableLiveData<List<ResponseResultCount>> data = new MutableLiveData<>();
        mApiService.getNYCSchoolCount(AppConstant.COUNT_QUERY_STRING).enqueue(
                new Callback<List<ResponseResultCount>>() {
                    @Override
                    public void onResponse(
                            Call<List<ResponseResultCount>> call,
                            Response<List<ResponseResultCount>> response ) {
                        if (response.isSuccessful()) {
                            data.setValue(response.body());
                        }
                    }

                    @Override
                    public void onFailure( Call<List<ResponseResultCount>> call, Throwable t ) {
                        data.setValue(null);
                    }
                });
        return data;
    }

    /**
     * Get the no of High school score records available in the NYC repo
     * 
     * @return High school score record count in NYC
     */
    public LiveData<List<ResponseResultCount>> getNYCSchoolScoreCount() {
        final MutableLiveData<List<ResponseResultCount>> data = new MutableLiveData<>();
        mApiService.getNYCSchoolScoreCount(AppConstant.COUNT_QUERY_STRING).enqueue(
                new Callback<List<ResponseResultCount>>() {
                    @Override
                    public void onResponse(
                            Call<List<ResponseResultCount>> call,
                            Response<List<ResponseResultCount>> response ) {
                        if (response.isSuccessful()) {
                            data.setValue(response.body());
                        }
                    }

                    @Override
                    public void onFailure( Call<List<ResponseResultCount>> call, Throwable t ) {
                        data.setValue(null);
                    }
                });
        return data;
    }
}
