
package com.santhana.nycschool.ui;

import com.santhana.nycschool.AppConstant;
import com.santhana.nycschool.NycApplication;
import com.santhana.nycschool.R;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;

/**
 * App-Splash screen activity.
 */
public class SplashActivity extends BaseActivity {

    private Handler mHandlerSplash = null;

    private Runnable mRunnable = null;

    @Override
    public void onCreate( @Nullable Bundle savedInstanceState ) {
        ((NycApplication) getApplication()).getApplicationComponent().inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mHandlerSplash = new Handler();
        mRunnable = () -> {
            startActivity(HomeActivity.getHomeIntent(SplashActivity.this));
            finish();
        };

    }

    @Override
    protected void onResume() {
        super.onResume();
        mHandlerSplash.postDelayed(mRunnable, AppConstant.SPLASH_DELAY_TIME);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mHandlerSplash.removeCallbacks(null);
    }

    @Override
    protected void onDestroy() {
        mHandlerSplash = null;
        mRunnable = null;
        super.onDestroy();
    }
}
