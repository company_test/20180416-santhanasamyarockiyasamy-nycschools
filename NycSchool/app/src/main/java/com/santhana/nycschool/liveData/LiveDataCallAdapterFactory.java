
package com.santhana.nycschool.liveData;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import android.arch.lifecycle.LiveData;

import com.santhana.nycschool.service.ApiResponse;
import com.santhana.nycschool.utils.Logger;

import retrofit2.CallAdapter;
import retrofit2.Retrofit;

/**
 * Factory to build adapters which will be used to convert the retrofit calls
 * into the custom types.
 */
public class LiveDataCallAdapterFactory extends CallAdapter.Factory {

    @Override
    public CallAdapter<?, ?> get( Type returnType, Annotation[] annotations, Retrofit retrofit ) {

        if (getRawType(returnType) != LiveData.class) {
            return null;
        }

        Type observableType = getParameterUpperBound(0, (ParameterizedType) returnType);

        Class<?> rawObservableType = getRawType(observableType);

        if (rawObservableType != ApiResponse.class) {
            throw new IllegalArgumentException("Type must be a resource!");
        }

        if (!(observableType instanceof ParameterizedType)) {
            throw new IllegalArgumentException("Resource must be parameterized!");
        }

        Type bodyType = getParameterUpperBound(0, (ParameterizedType) observableType);

        Logger.printDLog("Nyc", bodyType.toString());
        return new LiveDataCallAdapter<>(bodyType);
    }
}
