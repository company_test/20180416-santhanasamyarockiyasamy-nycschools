
package com.santhana.nycschool.viewmodel;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.santhana.nycschool.repo.NYCSchoolRepo;

import javax.inject.Singleton;

@Singleton
public class AppViewModelFactory implements ViewModelProvider.Factory {

    private final NYCSchoolRepo mNYCSchoolRepo;

    public AppViewModelFactory(NYCSchoolRepo aNYCSchoolRepo) {
        mNYCSchoolRepo = aNYCSchoolRepo;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(SchoolInfoViewModel.class)) {
            return (T) new SchoolInfoViewModel(mNYCSchoolRepo);
        } else if (modelClass.isAssignableFrom(ScoreInfoViewModel.class)) {
            return (T) new ScoreInfoViewModel(mNYCSchoolRepo);
        } else {
            throw new IllegalArgumentException("ViewModel Not Found");
        }
    }

}
