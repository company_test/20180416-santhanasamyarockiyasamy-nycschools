
package com.santhana.nycschool.ui;

import com.santhana.nycschool.NycApplication;
import com.santhana.nycschool.R;
import com.santhana.nycschool.databinding.ActivitySchoolDetailBinding;
import com.santhana.nycschool.models.School;
import com.santhana.nycschool.utils.AppUtils;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;

/**
 * Screen to show the individual school details
 */
public class SchoolDetailActivity extends BaseActivity {

    private static final String TAG = SchoolDetailActivity.class.getSimpleName();

    /**
     * Key to get the School details extra object from the Launch Intent.
     */
    private static final String BUNDLE_EXTRA_KEY_SCHOOL_INFO = "School Info";

    private ActivitySchoolDetailBinding mBinding;

    private School mSchoolInfo;

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        ((NycApplication) getApplication()).getApplicationComponent().inject(this);

        super.onCreate(savedInstanceState);

        mSchoolInfo = getIntent().getParcelableExtra(BUNDLE_EXTRA_KEY_SCHOOL_INFO);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_school_detail);

        mBinding.makeItFavImgView.setOnClickListener(
                v -> mBinding.makeItFavImgView
                        .setSelected(!mBinding.makeItFavImgView.isSelected()));
        mBinding.copyImgView
                .setOnClickListener(v -> AppUtils.showShortToast(R.string.str_not_implemented));

        mBinding.getSatScoreImgView
                .setOnClickListener(v -> AppUtils.showShortToast(R.string.str_not_implemented));
        mBinding.setSchoolInfo(mSchoolInfo);
    }

    @Override
    protected boolean isBackButtonNeeded() {
        return true;
    }

    @Override
    protected void configureActionBar() {
        super.configureActionBar();
        mActionBarLayout.setLeftTitle(R.string.str_title_school_info);
    }

    /**
     * Launch School Detail Activity
     *
     * @param context
     * @return
     */
    public static void launchSchoolDetail( Context context, School aSchoolInfo ) {
        Intent intent = new Intent(context, SchoolDetailActivity.class);
        intent.putExtra(BUNDLE_EXTRA_KEY_SCHOOL_INFO, aSchoolInfo);
        context.startActivity(intent);
    }

}
