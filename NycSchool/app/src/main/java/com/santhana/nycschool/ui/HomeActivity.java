
package com.santhana.nycschool.ui;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.santhana.nycschool.NycApplication;
import com.santhana.nycschool.R;
import com.santhana.nycschool.databinding.ActivityHomeBinding;
import com.santhana.nycschool.utils.AppUtils;
import com.santhana.nycschool.utils.UiUtils;

/**
 * Home screen activity which will act as the dashboard for the NYC application.
 */
public class HomeActivity extends BaseActivity {

    private static final String TAG = HomeActivity.class.getSimpleName();

    private ActivityHomeBinding mActivityHomeBinding;

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        ((NycApplication) getApplication()).getApplicationComponent().inject(this);

        super.onCreate(savedInstanceState);

        mActivityHomeBinding = DataBindingUtil.setContentView(this, R.layout.activity_home);

        UiUtils.setGrid(
                this,
                findViewById(R.id.row_1),
                R.string.dash_action_school_list,
                R.drawable.ic_dash_action_school_search,
                R.string.dash_action_sat_score,
                R.drawable.ic_dash_action_score);

        UiUtils.setGrid(
                this,
                findViewById(R.id.row_2),
                R.string.dash_action_fav_schools,
                R.drawable.ic_dash_action_fav_school,
                R.string.dash_action_contact_us,
                R.drawable.ic_dash_action_contact);

        UiUtils.setGrid(
                this,
                findViewById(R.id.row_3),
                R.string.dash_action_agency,
                R.drawable.ic_dash_action_start,
                R.string.dash_action_news,
                R.drawable.ic_dash_action_news);
    }

    @Override
    public void onClick( View aView ) {

        super.onClick(aView);
        if (null == aView.getTag()) {
            return;
        }
        int lTargetTagID = Integer.parseInt(aView.getTag().toString());

        if (R.string.dash_action_school_list == lTargetTagID) {
            startActivity(new Intent(this, SchoolListActivity.class));
        } else if (R.string.dash_action_sat_score == lTargetTagID) {
            startActivity(new Intent(this, ScoreListActivity.class));
        } else if (R.string.dash_action_fav_schools == lTargetTagID) {
            AppUtils.showShortToast(R.string.str_not_implemented);
        } else if (R.string.dash_action_contact_us == lTargetTagID) {
            AppUtils.showShortToast(R.string.str_not_implemented);
        } else if (R.string.dash_action_agency == lTargetTagID) {
            AppUtils.showShortToast(R.string.str_not_implemented);
        } else if (R.string.dash_action_news == lTargetTagID) {
            AppUtils.showShortToast(R.string.str_not_implemented);
        }
    }

    /**
     * Get Intent to launch Home
     *
     * @param context
     * @return
     */
    public static Intent getHomeIntent( Context context ) {
        Intent intent = new Intent(context, HomeActivity.class);
        intent.addFlags(
                Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK
                        | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }
}
