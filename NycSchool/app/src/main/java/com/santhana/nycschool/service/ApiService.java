
package com.santhana.nycschool.service;

import java.util.List;

import com.santhana.nycschool.models.School;
import com.santhana.nycschool.models.ResponseResultCount;
import com.santhana.nycschool.models.Score;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

import static com.santhana.nycschool.AppConstant.*;

/**
 * API-Service interface
 */
public interface ApiService {

    @GET(SCHOOL_INFO_URI)
    Call<List<School>> getNYCSchoolList();

    @GET(SCHOOL_INFO_FILTER_URI)
    Call<List<School>> getPaginatedNYCSchoolList(
            @Query(NYCSchoolFilter.SELECT) String aSelectionParam,
            @Query(NYCSchoolFilter.ORDER) String aSortOrder,
            @Query(NYCSchoolFilter.LIMIT) String aLimit,
            @Query(NYCSchoolFilter.OFFSET) String aOffset );

    @GET(SCHOOL_COUNT_URI)
    Call<List<ResponseResultCount>> getNYCSchoolCount( @Query("$query") String aQuery );

    @GET(SAT_SCORE_URI_STR)
    Call<List<Score>> getNYCSchoolScoreList();

    @GET(SAT_SCORE_BY_ID_URI)
    Call<List<Score>> getNYCSchoolScoreByID( @Query(NYCSchoolFilter.SCHOOL_DBN_ID) String aDBNId );

    @GET(SAT_SCORE_URI_STR)
    Call<List<Score>> getPaginatedNYCSchoolScoreList(
            @Query(NYCSchoolFilter.SELECT) String aSelectionParam,
            @Query(NYCSchoolFilter.ORDER) String aSortOrder,
            @Query(NYCSchoolFilter.LIMIT) String aLimit,
            @Query(NYCSchoolFilter.OFFSET) String aOffset );

    @GET(SAT_SCORE_COUNT_URI)
    Call<List<ResponseResultCount>> getNYCSchoolScoreCount( @Query("$query") String aQuery );

}
