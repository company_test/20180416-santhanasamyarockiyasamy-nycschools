
package com.santhana.nycschool.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.santhana.nycschool.R;
import com.santhana.nycschool.databinding.SchoolListItemBinding;
import com.santhana.nycschool.databinding.ScoreListItemBinding;
import com.santhana.nycschool.models.School;
import com.santhana.nycschool.models.Score;

import java.util.List;

public class ScoreListAdapter
        extends RecyclerView.Adapter<ScoreListAdapter.ScoreInfoViewHolder> {

    private List<Score> mScoreList = null;

    private Context mContext = null;

    private ResultsClickListener resultsClickListener = null;

    public interface ResultsClickListener {
        void onResultItemClicked(Score aScore);
    }

    public ScoreListAdapter(@NonNull Context context, ResultsClickListener resultsClickListener) {
        this.mContext = context;
        this.resultsClickListener = resultsClickListener;
    }

    public void setScoreList(List<Score> aScoreList) {
        this.mScoreList = aScoreList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ScoreInfoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ScoreInfoViewHolder(
                DataBindingUtil.inflate(
                        LayoutInflater.from(mContext),
                        R.layout.score_list_item,
                        parent,
                        false));
    }

    @Override
    public void onBindViewHolder(@NonNull ScoreInfoViewHolder holder, int position) {
        holder.resultsRowBinding.setScoreInfo(mScoreList.get(position));
    }

    @Override
    public int getItemCount() {
        return mScoreList == null || mScoreList.isEmpty() ? 0 : mScoreList.size();
    }

    public class ScoreInfoViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        private ScoreListItemBinding resultsRowBinding;

        public ScoreInfoViewHolder(ScoreListItemBinding resultsRowBinding) {
            super(resultsRowBinding.getRoot());
            this.resultsRowBinding = resultsRowBinding;
            resultsRowBinding.getRoot().setOnClickListener(this);
        }

        public void setScoreInfo(Score aScoreInfo) {
            resultsRowBinding.setScoreInfo(aScoreInfo);
        }

        @Override
        public void onClick(View v) {
            resultsClickListener.onResultItemClicked(mScoreList.get(getAdapterPosition()));
        }
    }
}
