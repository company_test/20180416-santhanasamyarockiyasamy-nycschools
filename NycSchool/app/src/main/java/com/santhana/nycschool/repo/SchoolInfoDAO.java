
package com.santhana.nycschool.repo;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.santhana.nycschool.models.School;
import com.santhana.nycschool.models.Score;

import java.util.List;

@Dao
/**
 * Data access object to manipulate the school details stored in the local DB
 */
public interface SchoolInfoDAO {

    @Query("SELECT * FROM School")
    LiveData<List<School>> getNYCHighSchools();

    @Query("SELECT * FROM School WHERE dbn = :aDBNID")
    Score getSchoolByID( String aDBNID );
}
