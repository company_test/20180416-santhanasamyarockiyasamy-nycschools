
package com.santhana.nycschool.viewmodel;

import java.util.List;

import com.santhana.nycschool.models.School;
import com.santhana.nycschool.models.ResponseResultCount;
import com.santhana.nycschool.repo.NYCSchoolRepo;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.VisibleForTesting;

public class SchoolInfoViewModel extends ViewModel {

    private NYCSchoolRepo mNYCSchoolRepo;

    private LiveData<List<School>> mSchoolListData;

    private LiveData<List<ResponseResultCount>> mSchoolCountData;


    public SchoolInfoViewModel(NYCSchoolRepo repo) {
        this.mNYCSchoolRepo = repo;

        /**
         * Get High School List from local DB (Single source of truth)
         */
        // mSchoolListData = repo.getNYCHighSchoolListFromDB();

        mSchoolListData = repo.getNYCSchoolList();

        mSchoolCountData = repo.getNYCSchoolCount();
    }

    @VisibleForTesting
    public LiveData<List<ResponseResultCount>> getSchoolResultSetSize() {
        return mSchoolCountData;
    }

    @VisibleForTesting
    public LiveData<List<School>> getSchoolList() {
        return mSchoolListData;
    }

    @VisibleForTesting
    public LiveData<List<School>> getFilteredSchoolList(int aOffset, int aLimit) {
        return mNYCSchoolRepo.getFilteredNYCSchoolList(aOffset, aLimit);
    }

}
