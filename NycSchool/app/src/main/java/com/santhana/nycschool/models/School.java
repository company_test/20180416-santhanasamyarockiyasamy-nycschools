
package com.santhana.nycschool.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

/**
 * Model to represent the NYC school info
 */
@Entity
public class School implements Parcelable {

    @NonNull
    @PrimaryKey
    private String dbn;

    private String bus;

    private String building_code;

    private String ell_programs;

    private String academicopportunities1;

    private String academicopportunities2;

    private String total_students;

    private String extracurricular_activities;

    private String directions1;

    private String finalgrades;

    private String website;

    private String boro;

    private String school_name;

    private String fax_number;

    private String school_sports;

    private String grades2018;

    private String pct_stu_safe;

    private String pct_stu_enough_variety;

    private String school_email;

    private String subway;

    private String school_10th_seats;

    private String overview_paragraph;

    private String school_accessibility_description;

    private String attendance_rate;

    private String location;

    private String phone_number;

    private String neighborhood;

    private String zip;

    private String start_time;

    private String end_time;

    public String getZip() {
        return zip;
    }

    public void setZip( String zip ) {
        this.zip = zip;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time( String start_time ) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time( String end_time ) {
        this.end_time = end_time;
    }

    public String getBus() {
        return bus;
    }

    public void setBus( String bus ) {
        this.bus = bus;
    }

    public String getBuilding_code() {
        return building_code;
    }

    public void setBuilding_code( String building_code ) {
        this.building_code = building_code;
    }

    public String getEll_programs() {
        return ell_programs;
    }

    public void setEll_programs( String ell_programs ) {
        this.ell_programs = ell_programs;
    }

    public String getAcademicopportunities1() {
        return academicopportunities1;
    }

    public void setAcademicopportunities1( String academicopportunities1 ) {
        this.academicopportunities1 = academicopportunities1;
    }

    public String getAcademicopportunities2() {
        return academicopportunities2;
    }

    public void setAcademicopportunities2( String academicopportunities2 ) {
        this.academicopportunities2 = academicopportunities2;
    }

    public String getTotal_students() {
        return total_students;
    }

    public void setTotal_students( String total_students ) {
        this.total_students = total_students;
    }

    public String getExtracurricular_activities() {
        return extracurricular_activities;
    }

    public void setExtracurricular_activities( String extracurricular_activities ) {
        this.extracurricular_activities = extracurricular_activities;
    }

    public String getDirections1() {
        return directions1;
    }

    public void setDirections1( String directions1 ) {
        this.directions1 = directions1;
    }

    public String getFinalgrades() {
        return finalgrades;
    }

    public void setFinalgrades( String finalgrades ) {
        this.finalgrades = finalgrades;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite( String website ) {
        this.website = website;
    }

    public String getBoro() {
        return boro;
    }

    public void setBoro( String boro ) {
        this.boro = boro;
    }

    public String getSchool_name() {
        return school_name;
    }

    public void setSchool_name( String school_name ) {
        this.school_name = school_name;
    }

    public String getFax_number() {
        return fax_number;
    }

    public void setFax_number( String fax_number ) {
        this.fax_number = fax_number;
    }

    public String getSchool_sports() {
        return school_sports;
    }

    public void setSchool_sports( String school_sports ) {
        this.school_sports = school_sports;
    }

    public String getGrades2018() {
        return grades2018;
    }

    public void setGrades2018( String grades2018 ) {
        this.grades2018 = grades2018;
    }

    public String getDbn() {
        return dbn;
    }

    public void setDbn( String dbn ) {
        this.dbn = dbn;
    }

    public String getPct_stu_safe() {
        return pct_stu_safe;
    }

    public void setPct_stu_safe( String pct_stu_safe ) {
        this.pct_stu_safe = pct_stu_safe;
    }

    public String getPct_stu_enough_variety() {
        return pct_stu_enough_variety;
    }

    public void setPct_stu_enough_variety( String pct_stu_enough_variety ) {
        this.pct_stu_enough_variety = pct_stu_enough_variety;
    }

    public String getSchool_email() {
        return school_email;
    }

    public void setSchool_email( String school_email ) {
        this.school_email = school_email;
    }

    public String getSubway() {
        return subway;
    }

    public void setSubway( String subway ) {
        this.subway = subway;
    }

    public String getSchool_10th_seats() {
        return school_10th_seats;
    }

    public void setSchool_10th_seats( String school_10th_seats ) {
        this.school_10th_seats = school_10th_seats;
    }

    public String getOverview_paragraph() {
        return overview_paragraph;
    }

    public void setOverview_paragraph( String overview_paragraph ) {
        this.overview_paragraph = overview_paragraph;
    }

    public String getSchool_accessibility_description() {
        return school_accessibility_description;
    }

    public void setSchool_accessibility_description( String school_accessibility_description ) {
        this.school_accessibility_description = school_accessibility_description;
    }

    public String getAttendance_rate() {
        return attendance_rate;
    }

    public void setAttendance_rate( String attendance_rate ) {
        this.attendance_rate = attendance_rate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation( String location ) {
        this.location = location;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number( String phone_number ) {
        this.phone_number = phone_number;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood( String neighborhood ) {
        this.neighborhood = neighborhood;
    }

    @Override
    public String toString() {
        return "ClassPojo [bus = " + bus + ", building_code = " + building_code
                + ", ell_programs = " + ell_programs + ", academicopportunities1 = "
                + academicopportunities1 + ", academicopportunities2 = " + academicopportunities2
                + ", total_students = " + total_students + ", extracurricular_activities = "
                + extracurricular_activities + ", directions1 = " + directions1 + ", finalgrades = "
                + finalgrades + ", website = " + website + ", boro = " + boro + ", school_name = "
                + school_name + ", fax_number = " + fax_number + ", school_sports = "
                + school_sports + ", grades2018 = " + grades2018 + ", dbn = " + dbn
                + ", pct_stu_safe = " + pct_stu_safe + ", pct_stu_enough_variety = "
                + pct_stu_enough_variety + ", school_email = " + school_email + ", subway = "
                + subway + ", school_10th_seats = " + school_10th_seats + ", overview_paragraph = "
                + overview_paragraph + ", school_accessibility_description = "
                + school_accessibility_description + ", attendance_rate = " + attendance_rate
                + ", location = " + location + ", phone_number = " + phone_number
                + ", neighborhood = " + neighborhood + "]";
    }

    @Override
    public boolean equals( Object o ) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        School nycSchool = (School) o;
        return dbn == nycSchool.dbn;
    }

    @Override
    public int hashCode() {
        return 31 * 17 + this.dbn.hashCode();
    }

    public School() {

    }

    protected School(Parcel in) {
        dbn = in.readString();
        bus = in.readString();
        building_code = in.readString();
        ell_programs = in.readString();
        academicopportunities1 = in.readString();
        academicopportunities2 = in.readString();
        total_students = in.readString();
        extracurricular_activities = in.readString();
        directions1 = in.readString();
        finalgrades = in.readString();
        website = in.readString();
        boro = in.readString();
        school_name = in.readString();
        fax_number = in.readString();
        school_sports = in.readString();
        grades2018 = in.readString();
        pct_stu_safe = in.readString();
        pct_stu_enough_variety = in.readString();
        school_email = in.readString();
        subway = in.readString();
        school_10th_seats = in.readString();
        overview_paragraph = in.readString();
        school_accessibility_description = in.readString();
        attendance_rate = in.readString();
        location = in.readString();
        phone_number = in.readString();
        neighborhood = in.readString();

        zip = in.readString();
        start_time = in.readString();
        end_time = in.readString();
    }

    public static final Creator<School> CREATOR = new Creator<School>() {
        @Override
        public School createFromParcel( Parcel in ) {
            return new School(in);
        }

        @Override
        public School[] newArray( int size ) {
            return new School[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel( Parcel dest, int flags ) {
        dest.writeString(dbn);
        dest.writeString(bus);
        dest.writeString(building_code);
        dest.writeString(ell_programs);
        dest.writeString(academicopportunities1);
        dest.writeString(academicopportunities2);
        dest.writeString(total_students);
        dest.writeString(extracurricular_activities);
        dest.writeString(directions1);
        dest.writeString(finalgrades);
        dest.writeString(website);
        dest.writeString(boro);
        dest.writeString(school_name);
        dest.writeString(fax_number);
        dest.writeString(school_sports);
        dest.writeString(grades2018);
        dest.writeString(pct_stu_safe);
        dest.writeString(pct_stu_enough_variety);
        dest.writeString(school_email);
        dest.writeString(subway);
        dest.writeString(school_10th_seats);
        dest.writeString(overview_paragraph);
        dest.writeString(school_accessibility_description);
        dest.writeString(attendance_rate);
        dest.writeString(location);
        dest.writeString(phone_number);
        dest.writeString(neighborhood);
        dest.writeString(zip);
        dest.writeString(start_time);
        dest.writeString(end_time);
    }
}
