
package com.santhana.nycschool.models;

import java.util.List;

/**
 * Model to represent the NYC school info list
 */
public class SchoolList {
    private List<School> schoolList;

    public List<School> getSchoolList() {
        return schoolList;
    }

    public void setEvents( List<School> schoolList ) {
        this.schoolList = schoolList;
    }

}
