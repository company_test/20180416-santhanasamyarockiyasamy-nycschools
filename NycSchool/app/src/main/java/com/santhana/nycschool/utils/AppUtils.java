
package com.santhana.nycschool.utils;

import android.net.Uri;
import android.widget.Toast;

import com.santhana.nycschool.AppConstant;
import com.santhana.nycschool.NycApplication;

public class AppUtils {

    public static String getNYCSchoolFilterURI( String aQueryParam ) {

        return Uri
                .parse(AppConstant.SCHOOL_INFO_URI)
                .buildUpon()
                .appendQueryParameter("dbn", aQueryParam)
                .build()
                .toString();
    }

    public static String getNYCSchoolFilterURI( int aOffset, int aLimit ) {

        return Uri
                .parse(AppConstant.SCHOOL_INFO_URI)
                .buildUpon()
                // .appendQueryParameter("dbn", aQueryParam)
                .appendQueryParameter("$offset", "" + aOffset)
                .appendQueryParameter("$limit", "" + aLimit)
                .appendQueryParameter("$order", ":id+ASC")
                .appendQueryParameter("$select", "dbn,school_name,boro,overview_paragraph")
                .build()
                .toString();
    }

    public static String getNYCSchoolFilterByIDURI( String aDBNID ) {

        return Uri
                .parse(AppConstant.SCHOOL_INFO_URI)
                .buildUpon()
                .appendQueryParameter("dbn", aDBNID)
                .appendQueryParameter("$order", ":id+ASC")
                .appendQueryParameter("$select", "dbn,school_name,boro,overview_paragraph")
                .build()
                .toString();
    }

    /**
     * Method to show toast message in short duration
     * 
     * @param aStringID
     */
    public static void showShortToast( int aStringID ) {
        Toast.makeText(AppConstant.APP_CONTEXT, aStringID, Toast.LENGTH_SHORT).show();
    }

    /**
     * Method to show toast message in short duration
     *
     * @param aMessage
     */
    public static void showShortToast( String aMessage) {
        Toast.makeText(AppConstant.APP_CONTEXT, aMessage, Toast.LENGTH_SHORT).show();
    }

    /**
     * Method to show toast message in long duration
     * 
     * @param aStringID
     */
    public static void showLongToast( int aStringID ) {

        Toast.makeText(AppConstant.APP_CONTEXT, aStringID, Toast.LENGTH_LONG).show();
    }

}
