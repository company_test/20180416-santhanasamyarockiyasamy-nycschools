
package com.santhana.nycschool.ui;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.santhana.nycschool.NycApplication;
import com.santhana.nycschool.R;
import com.santhana.nycschool.adapter.ScoreListAdapter;
import com.santhana.nycschool.databinding.ActivityScoreListBinding;
import com.santhana.nycschool.utils.AppUtils;
import com.santhana.nycschool.utils.Logger;
import com.santhana.nycschool.viewmodel.ScoreInfoViewModel;

import javax.inject.Inject;

/**
 * Screen to show the NYC High school SAT score in the list
 */
public class ScoreListActivity extends BaseActivity {

    private static final String TAG = SchoolListActivity.class.getSimpleName();

    @Inject
    ViewModelProvider.Factory mViewModelFactory;

    private ActivityScoreListBinding mBinding = null;

    private ScoreInfoViewModel mScoreInfoViewModel = null;

    private ScoreListAdapter mScoreListAdapter = null;

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        ((NycApplication) getApplication()).getApplicationComponent().inject(this);

        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_score_list);

        mScoreInfoViewModel = ViewModelProviders.of(this, mViewModelFactory).get(
                ScoreInfoViewModel.class);

        mScoreInfoViewModel.getScoreList().observe(this, scoreList -> {

            Logger.printILog(TAG, " Result Fetched -> " + scoreList.size());
        });

        mScoreInfoViewModel.getScoreResultSetSize().observe(this, resultCount -> {

            Logger.printILog(TAG, " Result Set Size -> " + resultCount.get(0).getResultCount());
        });

        mBinding.setViewModel(mScoreInfoViewModel);

        // Prepare adapter to bind the DataSet with UI
        mScoreListAdapter = new ScoreListAdapter(this, score -> {

            if (null == score) {
                return;
            }
            AppUtils.showShortToast("Yet to be Integrated!");
            // SchoolDetailActivity.launchSchoolDetail(this, nycSchool);
            Logger.printDLog(TAG, "Clicked Score Info " + score.toString());
        });

        // Configure Recycler view
        RecyclerView.LayoutManager lManager = new LinearLayoutManager(this);
        mBinding.scoreListView.setLayoutManager(lManager);
        // ItemDecoration for each item in the list
        mBinding.scoreListView
                .addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        mBinding.scoreListView.setAdapter(mScoreListAdapter);

        mBinding.setViewModel(mScoreInfoViewModel);

        mBinding.footerLayout.rightImgView.setEnabled(false);
        mBinding.footerLayout.leftImgView.setEnabled(false);
        mBinding.footerLayout.paginationTxtView.setText("");

        mBinding.footerLayout.rightImgView.setOnClickListener(this);
        mBinding.footerLayout.leftImgView.setOnClickListener(this);
        getSATScore();
    }

    @Override
    protected boolean isBackButtonNeeded() {
        return true;
    }

    @Override
    protected void configureActionBar() {
        super.configureActionBar();
        mActionBarLayout.setLeftTitle(R.string.dash_action_sat_score);
    }

    /**
     * Method to get the scores from the NYC repo
     */
    private void getSATScore() {

        CustomProgressDialog
                .showProgressBar(this, false, getString(R.string.str_warning_please_wait));
        mScoreInfoViewModel.getScoreList().observe(this, scoreList -> {

            if (null == scoreList) {
                CustomProgressDialog.hideProgressBar();
                AppUtils.showShortToast(R.string.str_warning_failed_to_get_score);
                return;
            }
            Logger.printDLog(TAG, " Result Fetched -> " + scoreList.size());
            mScoreListAdapter.setScoreList(scoreList);

            CustomProgressDialog.hideProgressBar();
        });
    }
}
