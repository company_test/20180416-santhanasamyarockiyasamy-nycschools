
package com.santhana.nycschool.liveData;

import android.arch.lifecycle.LiveData;

/**
 * Empty live data which will be used to handle the cases where the data set is
 * empty.
 * 
 * @param <D>
 */
public class AbsentLiveData<D> extends LiveData<D> {

    private AbsentLiveData() {
        postValue(null);
    }

    public static <T> LiveData<T> create() {
        // noinspection unchecked
        return new AbsentLiveData<T>();
    }
}
