### 20180416-SanthanasamyArockiyasamy-NYCSchools (JPMC - Test)

---
#### Supported Features
* Show NYC High School Directory
* School level results for New York City on the SAT exam
  <br>(Note: Home screen has other non-implemented interfaces. Time being those features should be ignored))

---
#### Features cab be integrated
* Mark it as Favorites, to add school into the favorite school list.
* Search school by school name/ ID.
* Provide filters on the school info.
* Share school info from the school detail screen.
* Launch school location on the map while tapping on the location icon.
* Directly Call school from the detail screen.
* Show favorited school list screen to see only the interested schools.
---
#### Know Issues
* School & Score details will be fetched as part of app initialization. This cab be lazily fetched.
* Pagination should be added SAT score list screen.
* Pagination issues in school list screen.
* School start & close time is not available for some school. Add proper message on those places.
* Making school as favorite is not working.
---
#### Application Screenshots

>Splash Screen

<img src="screen_shots/splash_screen.png"  width="350" height="520"/>

>DashBoard(Home) Screen

<img src="screen_shots/dash_board_screen.png"  width="350" height="520">

>NYC School's Screen

<img src="screen_shots/school_list_screen.png"  width="350" height="520">

>School Detail Screen

<img src="screen_shots/school_detail_screen.png"  width="350" height="520">

>NYC School SAT Score's

<img src="screen_shots/sat_score_screen.png"  width="350" height="520">

---